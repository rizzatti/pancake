# -*- coding: utf-8 -*-
import pytest

from pancake.parser import Parser, STATE_OUTSIDE_TASK_DEFINITION, STATE_INSIDE_TASK_DEFINITION, MissingTaskError
from pancake.task_manager import TaskManager


class TestParser(object):
    def setup_method(self, method):
        pass

    def teardown_method(self, method):
        TaskManager._instance = None

    def test_starts_at_initial_state(self):
        p = Parser("")
        assert p.state == STATE_OUTSIDE_TASK_DEFINITION

    def test_parses_simple_task_definition(self):
        Parser("a {").parse()
        assert TaskManager().tasks() == ['a']

    def test_parsing_simple_task_definition_changes_state(self):
        p = Parser("abcde    {")
        p.parse()
        assert p.state == STATE_INSIDE_TASK_DEFINITION

    def test_parsing_simple_full_task_definition_creates_a_task(self):
        Parser("abc {\n}").parse()
        assert TaskManager().tasks() == ['abc']

    def test_parsing_simple_full_task_definition_goes_back_to_initial_state(self):
        p = Parser("abc {\n}")
        p.parse()
        assert p.state == STATE_OUTSIDE_TASK_DEFINITION

    def test_parsing_task_with_commands(self):
        p = Parser("abc {\n  echo 1\n  echo 2\n  }  ")
        p.parse()
        task = TaskManager()._tasks["abc"]
        assert task.commands == ["echo 1", "echo 2"]

    def test_task_with_before(self):
        p = Parser("abc {\necho 1\n}\nabc.before {\necho 2\necho 3\n}\nabc.before {\necho 4\n}\n")
        p.parse()
        tm = TaskManager()
        assert tm.tasks() == ["abc"]
        assert tm._tasks["abc"].commands == ["echo 4", "echo 2", "echo 3", "echo 1"]

    def test_task_with_after(self):
        Parser("abc {\necho 1\n}\nabc.after {\necho 2\n}\n").parse()
        assert TaskManager()._tasks["abc"].commands == ["echo 1", "echo 2"]

    def test_task_with_before_and_after(self):
        Parser("abc {\necho 1\n}\nabc.after {\necho 2\n}\nabc.before {\necho 3\n}\n").parse()
        assert TaskManager()._tasks["abc"].commands == ["echo 3", "echo 1", "echo 2"]

    def test_before_task_without_main_task(self):
        with pytest.raises(MissingTaskError):
            Parser("abc.before {\necho 1\n}\n").parse()

    def test_task_with_dependency(self):
        Parser("abc:def {\necho 1\n}\ndef {\necho 2\n}\n").parse()
        assert TaskManager()._tasks["abc"].dependencies == ["def"]

    def test_task_with_multiple_dependencies(self):
        Parser("abc:x,y,z {\necho 1\n}\n").parse()
        assert TaskManager()._tasks["abc"].dependencies == ["x", "y", "z"]
