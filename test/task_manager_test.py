# -*- coding: utf-8 -*-
from pancake.task_manager import TaskManager
import subprocess


class TestTaskManager(object):
    def setup_method(self, method):
        pass

    def teardown_method(self, method):
        TaskManager._instance = None

    def test_it_is_a_singleton(self):
        t1 = TaskManager()
        t2 = TaskManager()
        assert t1._instance is t2._instance

    def test_has_no_tasks_initially(self):
        tm = TaskManager()
        assert tm.tasks() == []

    def test_adding_a_task(self):
        tm = TaskManager()
        tm.add_task("a", ["b", "c"])
        tm.build()
        assert tm.tasks() == ["a"]

    def test_adding_a_task_returns_the_task(self):
        tm = TaskManager()
        t = tm.add_task("a")
        tm.build()
        assert t.name == "a"

    def test_inconsistency(self):
        tm = TaskManager()
        tm.add_task("a", ["b", "c"])
        tm.build()
        assert not tm.consistent()

    def test_consistency(self):
        tm = TaskManager()
        tm.add_task("a", ["b", "c"])
        tm.add_task("b")
        tm.add_task("c")
        tm.build()
        assert tm.consistent()

    def test_add_command(self):
        tm = TaskManager()
        tm.add_task("a")
        tm.build()
        tm.add_command("a", "echo abc")
        assert tm._tasks["a"].commands == ["echo abc"]

    def test_execute(self, monkeypatch):
        tm = TaskManager()
        task_a = tm.add_task("a", ["b"])
        task_a.add_command("1")
        task_b = tm.add_task("b", ["c"])
        task_b.add_command("2")
        task_c = tm.add_task("c")
        task_c.add_command("3")
        tm.build()

        commands = []
        def mockreturn(command, *args, **kwargs):
            commands.append(command)

        monkeypatch.setattr(subprocess, 'run', mockreturn)

        tm.execute("a")
        assert commands == ["3", "2", "1"]
