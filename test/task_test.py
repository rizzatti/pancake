# -*- coding: utf-8 -*-
from pancake.task import Task
import subprocess

class TestTask(object):
    def test_simple_task_has_a_name(self):
        t = Task("a")
        assert t.name == "a"

    def test_simple_task_has_no_dependencies(self):
        t = Task("a")
        assert t.dependencies == []

    def test_simple_task_has_no_commands(self):
        t = Task("a")
        assert t.commands == []

    def test_adding_a_command_to_simple_task(self):
        t = Task("a")
        t.add_command("echo")
        assert t.commands == ["echo"]

    def test_adding_command_to_simple_task(self):
        t = Task("a")
        t.add_command("echo 1")
        t.add_command("echo 2")
        t.add_command("echo 3")
        assert t.commands == ["echo 1", "echo 2", "echo 3"]

    def test_creating_task_with_dependencies(self):
        t = Task("a", ["b", "c"])
        assert t.name == "a"
        assert t.dependencies == ["b", "c"]

    def test_add_before_command(self):
        t = Task("a")
        t.add_command("echo 1")
        t.add_before_command("echo 2")
        t.add_before_command("echo 3")
        assert t.commands == ["echo 3", "echo 2", "echo 1"]

    def test_add_after_command(self):
        t = Task("a")
        t.add_command("echo 1")
        t.add_after_command("echo 2")
        assert t.commands == ["echo 1", "echo 2"]

    def test_multiple_commands(self):
        t = Task("a")
        t.add_command("1")
        t.add_command("2")
        t.add_after_command("3")
        t.add_before_command("4")
        assert t.commands == ["4", "1", "2", "3"]

    def test_execute(self, monkeypatch):
        t = Task("a")
        t.add_command("1")
        t.add_command("2")
        t.add_after_command("3")
        t.add_before_command("4")

        commands = []
        def mockreturn(command, *args, **kwargs):
            commands.append(command)

        monkeypatch.setattr(subprocess, 'run', mockreturn)

        t.execute()
        assert commands == ["4", "1", "2", "3"]
