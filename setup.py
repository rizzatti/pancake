#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pip.download import PipSession
from pip.req import parse_requirements
from setuptools import setup, find_packages

requirements = parse_requirements('requirements.txt', session=PipSession())
setup(
    name='pancake',
    version='0.1.0',
    maintainer='José Otávio Rizzatti Ferreira',
    maintainer_email='joseotaviorf@gmail.com',
    url='https://bitbucket.org/rizzatti/tilde.git',
    license='MIT',
    packages=find_packages(),
    install_requires=[str(r.req) for r in requirements],
    setup_requires=['pytest-runner'],
    tests_require=['pytest==3.2.0'],
    zip_safe=False,
    include_package_data=True,
    entry_points={
        'console_scripts': ['pancake=pancake.cli:cli'],
    },
)
