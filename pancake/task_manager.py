# -*- coding: utf-8 -*-
import re

from .task import Task


R_SUBTASK = re.compile(r'\.(before|after)')


class TaskManager(object):
    class __Singleton(object):
        def __init__(self):
            self._tasks = {}
            self._temp = []

        def tasks(self):
            return list(self._tasks.keys())

        def add_task(self, *args, **kwargs):
            t = Task(*args, **kwargs)
            self._temp.append(t)
            return t

        def add_command(self, name, *args, **kwargs):
            t = self._tasks[name]
            t.add_command(*args, **kwargs)

        def consistent(self):
            known_deps = {dep for task in self._tasks.values() for dep in task.dependencies}
            known_tasks = set(self.tasks())
            return known_deps.issubset(known_tasks)

        def execute(self, name):
            t = self._tasks[name]
            for dep in t.dependencies:
                self.execute(dep)
            t.execute()

        def build(self):
            for task in sorted(self._temp, key=lambda x: x.name):
                match = R_SUBTASK.findall(task.name)
                if match:
                    name = re.sub(R_SUBTASK, '', task.name)
                    target = self._tasks[name]
                    subtype = match[0]
                    if subtype == 'before':
                        for command in reversed(task.commands):
                            target.add_before_command(command)
                    elif subtype == 'after':
                        for command in task.commands:
                            target.add_after_command(command)
                else:
                    self._tasks[task.name] = task
            self._temp = []


    _instance = None

    def __init__(self):
        if not TaskManager._instance:
            TaskManager._instance = TaskManager.__Singleton()

    def __getattr__(self, *args):
        return getattr(self._instance, *args)
