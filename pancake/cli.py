#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click

from pancake.project import new_project, check_environment, initialize
from pancake.task_manager import TaskManager


@click.group()
def cli():
    pass


@cli.command(help='Creates a new pancake project for Android')
@click.option('--package', default='com.example.myandroidapp',
              help='Package name (Reverse DNS). DEFAULT: com.example.myandroidapp')
@click.option('--platform', default='android-26', help='Android platform. DEFAULT: android-26')
@click.argument('project_name')
def new(package, platform, project_name):
    check_environment()
    click.echo('Creating project %s' % project_name)
    new_project(project_name, package, platform)
    click.echo('Project created')


@cli.command(help='Compiles the source code')
def compile():
    initialize()
    TaskManager().execute("compile")


@cli.command(help='Generates the APK file')
def package():
    initialize()
    TaskManager().execute("package")


@cli.command(help='Installs the app in a running emulator')
def install():
    initialize()
    TaskManager().execute("install")


@cli.command(help='Uninstalls the app from a running emulator')
def uninstall():
    initialize()
    TaskManager().execute("uninstall")


if __name__ == "__main__":
    cli()
