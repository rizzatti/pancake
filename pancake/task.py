# -*- coding: utf-8 -*-
import subprocess

class Task(object):
    def __init__(self, name, dependencies=None):
        self.name = name
        if dependencies is None:
            dependencies = []
        self.dependencies = list(dependencies)
        self.task_commands = []
        self.before_commands = []
        self.after_commands = []

    def add_command(self, command):
        self.task_commands.append(command)

    def add_before_command(self, command):
        self.before_commands.insert(0, command)

    def add_after_command(self, command):
        self.after_commands.append(command)

    @property
    def commands(self):
        return self.before_commands + self.task_commands + self.after_commands

    def execute(self):
        for command in self.commands:
            subprocess.run(command, shell=True, check=True)
