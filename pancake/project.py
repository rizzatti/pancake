# -*- coding: utf-8 -*-
from pathlib import Path
import os

from jinja2 import Environment, PackageLoader, select_autoescape

from .parser import Parser
from .task_manager import TaskManager

env = Environment(
    loader=PackageLoader('pancake', 'templates'),
    autoescape=select_autoescape(['xml'])
)


DIRECTORIES = (
    'bin',
    'docs',
    'lib',
    'obj',
    'res/drawable',
    'res/layout',
    'res/values',
    'src'
)


class ProjectAlreadyExistsError(Exception):
    pass


class MissingEnvironmentVariableError(Exception):
    pass


class NotPancakeProjectError(Exception):
    pass


class MissingTaskError(Exception):
    pass


def check_environment():
    if not os.getenv('JAVA_HOME'):
        raise MissingEnvironmentVariableError('Please set JAVA_HOME to use pancake')
    if not os.getenv('ANDROID_HOME'):
        raise MissingEnvironmentVariableError('Please set ANDROID_HOME to use pancake')


def create_directories(name, package):
    project_dir = create_project(name)
    create_basic_structure(project_dir)
    package_dir = create_package_structure(name, package, project_dir)
    return (project_dir, package_dir)


def create_project(name):
    current_dir = Path(os.getcwd())
    project_dir = current_dir / name
    try:
        project_dir.mkdir()
    except FileExistsError as exc:
        msg = "A project named '%s' already exists. Please choose another name or remove the previous project." % name
        raise ProjectAlreadyExistsError(msg) from exc
    return project_dir


def create_basic_structure(path):
    for d in DIRECTORIES:
        target = path / d
        target.mkdir(parents=True)


def create_package_structure(name, package, project_dir):
    package = package.replace('.', '/')
    target = project_dir / 'src' / package
    target.mkdir(parents=True)
    return target


def populate_templates(name, package, project_dir, package_dir):
    create_android_manifest(name, package, project_dir)
    create_strings_resource(name, project_dir)
    copy_logo_resource(project_dir)
    create_java_class(name, package, package_dir)


def create_android_manifest(name, package, project_dir):
    template = env.get_template('AndroidManifest.xml')
    target = project_dir / 'AndroidManifest.xml'
    with target.open('w') as output:
        data = template.render(package=package, project_name=name)
        output.write(data)


def create_strings_resource(name, project_dir):
    template = env.get_template('strings.xml')
    target = project_dir / 'res/values/strings.xml'
    with target.open('w') as output:
        data = template.render(project_name=name)
        output.write(data)


def copy_logo_resource(project_dir):
    template_dir = Path(__file__).parent / 'templates'
    logo = template_dir / 'logo.png'
    target = project_dir / 'res/drawable/logo.png'
    with target.open('wb') as output:
        with logo.open('rb') as input:
            output.write(input.read())


def create_java_class(name, package, package_dir):
    template = env.get_template('HelloAndroid.java')
    target = package_dir / 'HelloAndroid.java'
    with target.open('w') as output:
        data = template.render(package=package, project_name=name)
        output.write(data)


def create_pancakefile(name, package, platform, project_dir):
    template = env.get_template('Pancakefile')
    target = project_dir / 'Pancakefile'
    with target.open('w') as output:
        data = template.render(project_name=name, platform=platform, package=package)
        output.write(data)


def new_project(project_name, package, platform):
    project_dir, package_dir = create_directories(project_name, package)
    create_pancakefile(project_name, package, platform, project_dir),
    populate_templates(project_name, package, project_dir, package_dir)


def initialize():
    check_environment()
    pancakefile = inside_pancake_directory()
    populate_tasks(pancakefile)


def inside_pancake_directory():
    current_dir = Path(os.getcwd())
    pancakefile = current_dir / "Pancakefile"
    if not pancakefile.exists():
        raise NotPancakeProjectError("Must be in the root directory of a pancake project.")
    return pancakefile


def populate_tasks(pancakefile):
    with pancakefile.open('r') as input:
        data = input.read()
        parser = Parser(data).parse()
    if not TaskManager().consistent():
        raise MissingTaskError("A task or more are missing from the Pancakefile. Cannot continue.")
