# -*- coding: utf-8 -*-
import re

from .task_manager import TaskManager

STATE_OUTSIDE_TASK_DEFINITION = 0
STATE_INSIDE_TASK_DEFINITION = 1

R_TASK_BEGIN = re.compile(r'(?P<name>\w+(\.(before|after))?)\s*(:\s*(?P<deps>[,\w]+))?\s*{')
R_TASK_END = re.compile(r'\s*}')


class MissingTaskError(Exception):
    pass


class Parser(object):
    def __init__(self, data):
        self.data = str(data)
        self.state = STATE_OUTSIDE_TASK_DEFINITION

    def parse(self):
        manager = TaskManager()
        task = None
        for line in self.data.splitlines():
            if self.state == STATE_OUTSIDE_TASK_DEFINITION:
                match = R_TASK_BEGIN.match(line)
                if match:
                    groups = match.groupdict()
                    name = groups['name']
                    dependencies = groups.get('deps', [])
                    if dependencies:
                        dependencies = dependencies.split(',')
                    task = manager.add_task(name, dependencies)
                    self.state = STATE_INSIDE_TASK_DEFINITION
            elif self.state == STATE_INSIDE_TASK_DEFINITION:
                match = R_TASK_END.match(line)
                if match:
                    self.state = STATE_OUTSIDE_TASK_DEFINITION
                    task = None
                else:
                    task.add_command(line.strip())
        try:
            manager.build()
        except KeyError as e:
            msg = "Task definition for task '{}' is missing".format(e.args[0])
            raise MissingTaskError(msg)
