# Pancake

Pancake is a build system for Android apps developed in Python.

## Requirements

* Python 3.5
* Java SDK and Android SDK

## Installation

Pancake was developed using Python 3.5, but is probably usable in any
version >= 3.4.

To install pancake, simply run:

```bash
git clone https://bitbucket.org/rizzatti/pancake.git
cd pancake
pip install .
```

The ``pancake`` executable should now be available on the system.

## Usage

Pancake makes a few assumptions about how it is going to be used. They
are:

* the Java SDK is installed and ``$JAVA_HOME`` is set correctly.
* the Android SDK is installed and ``$ANDROID_HOME`` is set
  appropriately.
* OS images, platforms and other downloadable content is managed by the
  user, not Pancake.
* AVDs and related emulator settings are managed by the user as well.

Pancake has 5 commands available: new, compile, package, install and
uninstall. The help is easily accessible using ``pancake --help``.

### Commands

#### new

Creates the project structure for a new Android app, with a few
resource files already in place, a logo, a simple "Hello World" screen
and predefined tasks.

``new`` can be called from any directory in the system, and only
requires a project name, but we recommend providing both the package
name and platform used (The platform should be downloaded either in
Android Studio or using the CLI tools).

More info and defaults available by running ``pancake new --help``.


#### compile, package, install and uninstall

Once the project is created, pancake should be ran from inside the
project root.

These 4 commands are available by executing ``pancake <COMMAND>``.

``compile`` will build the Java sources.

``package`` will build the sources, generated the DEX file, the unsigned
APK, create a debug keystore, sign the APK using the debug key and
finally zip align the final package.

``install`` will install the aligned APK to a running emulator instance.
Getting an emulator up and running is outside the scope of pancake.

``uninstall`` will uninstall the package from the emulator.


### The Pancakefile

Inside the project directory, pancake creates a file called
``Pancakefile``.

This file defines all the tasks required to build, package and launch
the Android app the project is creating.

But, this file is also extensible, and the user can hook himself to any
of the described phases and execute arbitrary shell commands at any
given point.

The file describes many tasks with the following syntax:

```
task1 {
shell command 1
shell command 2
}
```

The code above describes a task called ``task1`` with 2 shell commands
attached to it. The task can have as many shell commands as it desires
or needs.

After a task is defined (as a matter of fact, even before the task is
defined), task hooks can be created to either execute commands before
or after the task main body is ran.

```
task1.before {
shell command that will run before "shell command 1"
}
```

The hooks are ``task.before`` and ``task.after`` for a given ``task``,
and like the main task definition, any number of shell commands can be
issued.

More than one of these hooks can be attached to the same task. After
hoooks are appended to the list of commands, executing in the order
they are defined in, while Before hooks are prepended, executing in the
reverse order they are declared in. This might seem confusing, so let's
look at an example.

```
task1 {
echo 1
}

task1.before {
echo 2
}

task1.before {
echo 3
}

task1.after {
echo 4
}

task1.after {
echo 5
}
```

When executing ``task1``, the output from the console would be:

```
3
2
1
4
5
```

Last, but not least, tasks can have dependencies and that's how they
connect to one another. The syntax for it is shown bellow:

```
task1 {
echo 1
}

task2: task1 {
echo 2
}

task3 {
echo 3
}

task4: task3,task2 {
echo 4
}
```

In this scenario, task2 depends on task1, and task4 depends on both
task3 and task2.


## About the project

The project was built in Python and tries to be very minimal. It only
installs 3 external packages from pypi, one to help with templating,
another to enhance the CLI experience and the testing framework.

The core of the project is made of a Parser for the task definition
files, Tasks and a Task Manager that keeps track of tasks declarations,
potentially missing task definitions and dependencies among tasks. All
these core elements have extensive unit tests.

The 'project' module defines a collection of helper functions that will
be used to create the project structure and populate the templates.

The task definition file has extended syntax from the problem proposal,
allowing not only before and after hooks, but also to define task
dependencies and run complex shell code.

For these reasons, most of the commands accepted by pancake were
themselves implemented inside that same task definition/build steps
file.

The built-in commands could have been parsed and "kept" inside the
program, but I decided to expose them in the project Pancakefile to
showcase the syntax and types of shell constructs possible.

At some level, the program becomes a hybrid of Python and Shell script,
were python was used to define and understand these task definitions and
the order of execution, while the things that make it specific to
Android are implemented in the tasks themselves. This potentially shows
that the system could be easily adapted for other contexts.

The only command in which all actions are implemented in python is the
new command. The reason for that change is the higher complexity it has,
handling personalized directory strucure creation and templates.

One thing of notice in the task dependency system is that pancake does
not track wether a given task executed already or not. It simply
follows the dependency graph. So, it is possible to create either
infinite loops or diamond shaped dependency graphs like 2->1, 3->1 and
4->2,3. In this scenario, 1 would be run twice.

As a final note, I personally had never created a Android app before,
nor dealt with it's SDK. The project was tested only against the very
simple Hello World application provided and was able to run on the
emulator. Further changes to the project and rebuilds were reflected on
following deploys.

The steps defined to build an app have been adapted from [the guide at
the problem propostal](http://geosoft.no/development/android.html), as
some of those executables have moved around. That's the reason some of
the task definitions might seem complex.
